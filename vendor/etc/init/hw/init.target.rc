
# Copyright (c) 2019-2021, The Linux Foundation. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above
#       copyright notice, this list of conditions and the following
#       disclaimer in the documentation and/or other materials provided
#       with the distribution.
#     * Neither the name of The Linux Foundation nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#
# Changes from Qualcomm Innovation Center are provided under the following license:
# Copyright (c) 2022 Qualcomm Innovation Center, Inc. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted (subject to the limitations in the
# disclaimer below) provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#
#     * Redistributions in binary form must reproduce the above
#       copyright notice, this list of conditions and the following
#       disclaimer in the documentation and/or other materials provided
#       with the distribution.
#
#     * Neither the name of Qualcomm Innovation Center, Inc. nor the names of
#       its contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE
# GRANTED BY THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT
# HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
# GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
# IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import /vendor/etc/init/hw/init.qti.kernel.rc

on early-init
    write /proc/sys/kernel/printk_devkmsg ratelimited
    export MEMTAG_OPTIONS off

on init
    # Scheduler uclamp
    # Uclamp settings are being done by default in /system/core/rootdir/init.rc,
    # and there is no need to overwrite or duplicate following groups here.
    mkdir /dev/cpuctl/vip
    mkdir /dev/cpuctl/graphics
    mkdir /dev/cpuctl/boost

    chown system system /dev/cpuctl/vip
    chown system system /dev/cpuctl/graphics
    chown system system /dev/cpuctl/boost
    chown system system /dev/cpuctl/vip/tasks
    chown system system /dev/cpuctl/graphics/tasks
    chown system system /dev/cpuctl/boost/tasks
    chown system system /dev/cpuctl/vip/cgroup.procs
    chown system system /dev/cpuctl/graphics/cgroup.procs
    chown system system /dev/cpuctl/boost/cgroup.procs
    chown system system /dev/cpuctl/vip/psi.group_name_id
    chown system system /dev/cpuctl/graphics/psi.group_name_id

    chmod 0664 /dev/cpuctl/vip/tasks
    chmod 0664 /dev/cpuctl/graphics/tasks
    chmod 0664 /dev/cpuctl/boost/tasks
    chmod 0664 /dev/cpuctl/vip/cgroup.procs
    chmod 0664 /dev/cpuctl/graphics/cgroup.procs
    chmod 0664 /dev/cpuctl/boost/cgroup.procs
    chmod 0664 /dev/cpuctl/vip/psi.group_name_id
    chmod 0664 /dev/cpuctl/graphics/psi.group_name_id

    write /dev/cpuctl/vip/cpu.uclamp.colocate 1
    write /dev/cpuctl/vip/cpu.uclamp.min 0.1
    write /dev/cpuctl/vip/cpu.uclamp.sched_boost_no_override 1
    write /dev/cpuctl/vip/psi.group_name_id 91
    write /dev/cpuctl/graphics/psi.group_name_id 92

    # game schedule tunables
    mkdir /dev/cpuctl/game
    chown system system /dev/cpuctl/game
    chown system system /dev/cpuctl/game/tasks
    chown system system /dev/cpuctl/game/cgroup.procs
    chmod 0664 /dev/cpuctl/game/tasks
    chmod 0664 /dev/cpuctl/game/cgroup.procs
    write /dev/cpuctl/game/cpu.uclamp.min 40
    write /dev/cpuctl/game/cpu.uclamp.latency_sensitive 1

    mkdir /dev/cpuctl/limit
    chown system system /dev/cpuctl/limit
    chown system system /dev/cpuctl/limit/tasks
    chown system system /dev/cpuctl/limit/cgroup.procs
    chmod 0664 /dev/cpuctl/limit/tasks
    chmod 0664 /dev/cpuctl/limit/cgroup.procs

    #for cpu control bg_limit group
    mkdir /dev/cpuctl/bg_limit
    chown system system /dev/cpuctl/bg_limit/
    chown system system /dev/cpuctl/bg_limit/tasks
    chown system system /dev/cpuctl/bg_limit/cgroup.procs
    chmod 0664 /dev/cpuctl/bg_limit/tasks
    chmod 0664 /dev/cpuctl/bg_limit/cgroup.procs
    write /dev/cpuctl/bg_limit/cpu.uclamp.max 20
    write /dev/cpuctl/bg_limit/cpu.shares 208

    mkdir /dev/cpuset/game
    chown system system /dev/cpuset/game
    chown system system /dev/cpuset/game/tasks
    chown system system /dev/cpuset/game/cgroup.procs
    chmod 0664 /dev/cpuset/game/tasks
    chmod 0664 /dev/cpuset/game/cgroup.procs
    write /dev/cpuset/game/mems 0

    mkdir /dev/cpuset/boost
    chown system system /dev/cpuset/boost
    chown system system /dev/cpuset/boost/tasks
    chown system system /dev/cpuset/boost/cgroup.procs
    chmod 0664 /dev/cpuset/boost/tasks
    chmod 0664 /dev/cpuset/boost/cgroup.procs
    write /dev/cpuset/boost/cpus 0-7
    write /dev/cpuset/boost/mems 0

    # vip is for vip tasks
    mkdir /dev/cpuset/vip
    chown system system /dev/cpuset/vip
    chown system system /dev/cpuset/vip/tasks
    chown system system /dev/cpuset/vip/cgroup.procs
    chmod 0664 /dev/cpuset/vip/tasks
    chmod 0664 /dev/cpuset/vip/cgroup.procs
    write /dev/cpuset/vip/cpus 0-7
    write /dev/cpuset/vip/mems 0

    # set graphics cpuset
    mkdir /dev/cpuset/graphics
    chown system system /dev/cpuset/graphics
    chown system system /dev/cpuset/graphics/tasks
    chown system system /dev/cpuset/graphics/cgroup.procs
    chmod 0664 /dev/cpuset/graphics/tasks
    chmod 0664 /dev/cpuset/graphics/cgroup.procs
    write /dev/cpuset/graphics/cpus 0-6
    write /dev/cpuset/graphics/mems 0

    # set high-fg cpuset
    mkdir /dev/cpuset/high-fg
    write /dev/cpuset/high-fg/cpus 0-7
    write /dev/cpuset/high-fg/mems 0
    chown system system /dev/cpuset/high-fg
    chown system system /dev/cpuset/high-fg/cpus
    chown system system /dev/cpuset/high-fg/tasks
    chown system system /dev/cpuset/high-fg/cgroup.procs
    chmod 0664 /dev/cpuset/high-fg/cpus
    chmod 0664 /dev/cpuset/high-fg/tasks
    chmod 0664 /dev/cpuset/high-fg/cgroup.procs

    # set low-fg cpuset
    mkdir /dev/cpuset/low-fg
    write /dev/cpuset/low-fg/cpus 0-7
    write /dev/cpuset/low-fg/mems 0
    chown system system /dev/cpuset/low-fg
    chown system system /dev/cpuset/low-fg/cpus
    chown system system /dev/cpuset/low-fg/tasks
    chown system system /dev/cpuset/low-fg/cgroup.procs
    chmod 0664 /dev/cpuset/low-fg/cpus
    chmod 0664 /dev/cpuset/low-fg/tasks
    chmod 0664 /dev/cpuset/low-fg/cgroup.procs

    chown system system /dev/cpuctl/top-app/psi.group_name_id
    chmod 0664 /dev/cpuctl/top-app/psi.group_name_id
    write /dev/cpuctl/top-app/psi.group_name_id 93

    wait /dev/block/platform/soc/${ro.boot.bootdevice}
    symlink /dev/block/platform/soc/${ro.boot.bootdevice} /dev/block/bootdevice
    chown system system /sys/devices/platform/soc/1d84000.ufshc/auto_hibern8
    chmod 0660 /sys/devices/platform/soc/1d84000.ufshc/auto_hibern8
    start logd


on early-fs
    start vold

on fs
    start hwservicemanager
    mount_all /vendor/etc/fstab.qcom --early
    chown root system /mnt/vendor/persist
    chmod 0771 /mnt/vendor/persist
    restorecon_recursive /mnt/vendor/persist
    mkdir /mnt/vendor/persist/secnvm 0770 system system
    mkdir /mnt/vendor/persist/data 0700 system system

on property:ro.boot.product.vendor.sku=kalama
    setprop ro.soc.model kalama

on post-fs
    # set RLIMIT_MEMLOCK to 64MB
    setrlimit 8 67108864 67108864
    #Execute virtualization manager
    enable vendor.qvirtmgr
    start  vendor.qvirtmgr

on late-fs
    wait_for_prop hwservicemanager.ready true
    #exec_start wait_for_keymaster
    mount_all /vendor/etc/fstab.qcom --late

on post-fs-data
    mkdir /vendor/data/tombstones 0771 system system
    # Enable WLAN cold boot calibration
    write /sys/kernel/cnss/fs_ready 1

on early-boot
    start vendor.sensors

on boot
    write /dev/cpuset/audio-app/cpus 1-2
    # Add a cpuset for the camera daemon
    # We want all cores for camera
    mkdir /dev/cpuset/camera-daemon
    write /dev/cpuset/camera-daemon/cpus 0-7
    write /dev/cpuset/camera-daemon/mems 0
    chown cameraserver cameraserver /dev/cpuset/camera-daemon
    chown cameraserver cameraserver /dev/cpuset/camera-daemon/tasks
    chmod 0660 /dev/cpuset/camera-daemon/tasks
    chown system /sys/devices/platform/soc/990000.i2c/i2c-0/0-0038/trusted_touch_enable
    chmod 0660 /sys/devices/platform/soc/990000.i2c/i2c-0/0-0038/trusted_touch_enable
    chown system /sys/devices/platform/soc/990000.spi/spi_master/spi0/spi0.0/trusted_touch_enable
    chmod 0660 /sys/devices/platform/soc/990000.spi/spi_master/spi0/spi0.0/trusted_touch_enable
    chown system /sys/devices/system/cpu/hyp_core_ctl/enable
    chown system /sys/devices/system/cpu/hyp_core_ctl/hcc_min_freq
    chown system system /dev/cpuctl/vip/memory.pressure
    chmod 0664 /dev/cpuctl/vip/memory.pressure

on init && property:ro.boot.mode=charger
    wait_for_prop vendor.all.modules.ready 1
    mount_all /vendor/etc/charger_fw_fstab.qti --early
    wait /sys/kernel/boot_adsp/boot
    write /sys/kernel/boot_adsp/boot 1

on charger
    start vendor.power_off_alarm
    write /sys/kernel/cnss/charger_mode 1

#service vendor.lowi /vendor/bin/sscrpcd
#   class core
#   user system
#   group system wakelock
#   capabilities BLOCK_SUSPEND

#pd-mapper pd dump is disabled in beta,but enable in commercial
service vendor.pd_mapper /vendor/bin/pd-mapper 3
    class core
    user system
    group system

#Peripheral manager
service vendor.per_mgr /vendor/bin/pm-service
    class core
    user system
    group system
    ioprio rt 4

service vendor.per_proxy /vendor/bin/pm-proxy
    class core
    user system
    group system
    disabled

service vendor.mdm_helper /vendor/bin/mdm_helper
    class core
    group system wakelock
    disabled

service vendor.mdm_launcher /vendor/bin/sh /vendor/bin/init.mdm.sh
    class core
    oneshot

on property:init.svc.vendor.per_mgr=running
    start vendor.per_proxy

on property:sys.shutdown.requested=*
    write /sys/kernel/qcom_rproc/shutdown_in_progress 1
    stop vendor.per_proxy

on property:vold.decrypt=trigger_restart_framework
   start vendor.cnss_diag

service vendor.cnss_diag /system/vendor/bin/cnss_diag -q -f -t HELIUM
   class main
   user system
   group system wifi inet sdcard_rw media_rw diag
   oneshot

on early-boot && property:persist.vendor.pcie.boot_option=*
    write /sys/bus/platform/devices/1c00000.qcom,pcie/debug/boot_option ${persist.vendor.pcie.boot_option}
    write /sys/bus/platform/devices/1c08000.qcom,pcie/debug/boot_option ${persist.vendor.pcie.boot_option}

on property:sys.boot_completed=1 && property:msc.perf.fas_enable=true
    #fas init
    write /sys/kernel/fas/mgr/fas_enable 1
    write /sys/kernel/fas/boost/cluster0/best_capa_cluster 1344000
    write /sys/kernel/fas/boost/cluster1/best_capa_cluster 1536000
    write /sys/kernel/fas/boost/cluster2/best_capa_cluster 998400

on property:sys.boot_completed=1 && property:msc.perf.fas_enable=1
    #fas init
    write /sys/kernel/fas/mgr/fas_enable 1
    write /sys/kernel/fas/boost/cluster0/best_capa_cluster 1344000
    write /sys/kernel/fas/boost/cluster1/best_capa_cluster 1536000
    write /sys/kernel/fas/boost/cluster2/best_capa_cluster 998400

service fan_control_turn_on /vendor/bin/fan_service 1
    class late_start
    user root
    disabled
    oneshot

service fan_control_turn_off /vendor/bin/fan_service 0
    class late_start
    user root
    disabled
    oneshot
